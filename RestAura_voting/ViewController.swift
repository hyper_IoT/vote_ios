//
//  ViewController.swift
//  RestAura_voting
//
//  Created by Manuel Betancurt on 9/2/17.
//  Copyright © 2017 Manuel Betancurt. All rights reserved.
//

import AFNetworking
import UIKit
import SDWebImage

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var table: UITableView!

    var businesses: [Business]? = nil
    var selectedBusiness : Business?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false;

        
    }

    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Business.searchWithTerm(term: "Restaurants", completion: { (businesses: [Business]?, error: Error?) -> Void in
            self.businesses = businesses!
            
            self.fetchVotedRestaurants()
        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if (self.businesses?.count == nil) {
            return 0
        }else{
            return (self.businesses?.count)!
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cellInfo = self.businesses?[indexPath.row]
        
        let cell:RestaurantTableViewCell = self.table.dequeueReusableCell(withIdentifier: "cell") as! RestaurantTableViewCell

        cell.restaurantLabel.text = cellInfo?.name
        cell.restaurantImage.sd_setImage(with: cellInfo?.imageURL)
        cell.address.text = cellInfo?.address
        cell.votes.text = cellInfo?.votes?.stringValue

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.table .deselectRow(at: indexPath, animated: true)
        let cellInfo = self.businesses?[indexPath.row]
        
        selectedBusiness = cellInfo!

        let alertController = UIAlertController(title: "Vote for Restaurant", message: cellInfo?.name, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Vote", style: .default, handler: {
            alert -> Void in
            let textField = alertController.textFields![0] as UITextField
            self.dealWithUserVote(userName: textField.text!)
            
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "user name"
        })
        
        self.present(alertController, animated: true, completion: nil)


    }

    func dealWithUserVote(userName : String) {
        
        if userName.characters.count > 0 {
            registerVote(userName: userName)
            
        }else{
            showAlert(alertMessage: "Please enter your username to register your vote")
        }

    }
    
    func registerVote(userName:String) {

        let params = ["userName":userName,
                      "restaurant":selectedBusiness?.name!]

        let urlString = "https://4cnxmfijbh.execute-api.us-east-1.amazonaws.com/dev"
        
        let manager = AFHTTPSessionManager()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.responseSerializer = AFHTTPResponseSerializer()
        manager.post(urlString, parameters: params, success:
            {
                requestOperation, response in
                
                let result = NSString(data: (response as! NSData) as Data, encoding: String.Encoding.utf8.rawValue)!
                
                print(result)
                
                self.fetchVotedRestaurants()
        },
                     failure:
            {
                requestOperation, error in
        })
        
        
    }
    
    
    func fetchVotedRestaurants() {
        let manager = AFHTTPSessionManager()
        manager.get(
            "https://4cnxmfijbh.execute-api.us-east-1.amazonaws.com/dev/votes",
            parameters: nil,
            success:
            {
                (operation, responseObject) in
                
                let responseObjectDicto = (responseObject as AnyObject) as! NSDictionary

                for (biz) in self.businesses!{

                    let vote = responseObjectDicto[biz.name!] as? NSNumber
 
                    
                    if(vote != nil){
                        biz.votes = vote
                    }
                    
                }
                self.table .reloadData()
                
        },
            failure:
            {
                (operation, error) in
                print("Error: " + error.localizedDescription)
        })
    }
    
    func showAlert(alertMessage : String)   {
        let alertController = UIAlertController(title: "RestAura voting", message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            print("OK")
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}

