//
//  RestaurantTableViewCell.swift
//  RestAura_voting
//
//  Created by Manuel Betancurt on 9/2/17.
//  Copyright © 2017 Manuel Betancurt. All rights reserved.
//

import UIKit

class RestaurantTableViewCell: UITableViewCell {

    
    @IBOutlet weak var votes: UILabel!
    @IBOutlet weak var restaurantLabel: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var restaurantImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
