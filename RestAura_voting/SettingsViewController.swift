//
//  SettingsViewController.swift
//  RestAura_voting
//
//  Created by Manuel Betancurt on 10/2/17.
//  Copyright © 2017 Manuel Betancurt. All rights reserved.
//

import AFNetworking
import UIKit

import MessageUI


class SettingsViewController: UIViewController, MFMessageComposeViewControllerDelegate {

    @IBOutlet weak var eatingAtLabel: UILabel!
    @IBOutlet weak var SMSlabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Settings"

        // Do any additional setup after loading the view.
        
        
        
    }
    
    
    @IBAction func notificationSMStoPressed(_ sender: Any) {
        let alertController = UIAlertController(title: "Vote for Restaurant", message: "enter 1 number to notify", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Add", style: .default, handler: {
            alert -> Void in
            let textField = alertController.textFields![0] as UITextField
            
            self.SMSlabel.text = textField.text
            
            print(textField)
            
//            self.dealWithUserVote(userName: textField.text!)
            
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "user phone number"
            textField.keyboardType = UIKeyboardType.numberPad
        })
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func reachedButtonPressed(_ sender: Any) {
    
        print("reached")
        
        if (SMSlabel.text == "empty") {
            showAlert(alertMessage: "Please enter a number to notify")
        }else{
            //who was the winner?
            fetchVotedRestaurants()
        }
        
        
        

        
        //clear DB
    }

    
    func fetchVotedRestaurants() {
        let manager = AFHTTPSessionManager()
        manager.get(
            "https://4cnxmfijbh.execute-api.us-east-1.amazonaws.com/dev/votes",
            parameters: nil,
            success:
            {
                (operation, responseObject) in
                
                let responseObjectDicto = (responseObject as AnyObject) as! NSDictionary
                let orderedDicto = responseObjectDicto.sorted(by: { (a, b) in (a.value as! Double) > (b.value as! Double) })
                
                
                for (key,value) in orderedDicto {
                    print("\(key) = \(value)")
                    
                    self.eatingAtLabel.text = "\(key) with \(value) votes"
                    
                    //send notification
                    self.sendSMS()
                    
                    break
                }
 
                
        },
            failure:
            {
                (operation, error) in
                print("Error: " + error.localizedDescription)
        })
    }
    
    
    func sendSMS() {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = "We are eating at: \(self.eatingAtLabel.text!)"
            controller.recipients = [self.SMSlabel.text!]
            controller.messageComposeDelegate = self
            
            
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }
 
 
    
    func showAlert(alertMessage : String)   {
        let alertController = UIAlertController(title: "RestAura voting", message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            print("OK")
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    @IBAction func clearVotesButtonPressed(_ sender: Any) {
        
        let manager = AFHTTPSessionManager()
        manager.get(
            "https://4cnxmfijbh.execute-api.us-east-1.amazonaws.com/dev/deletevotes",
            parameters: nil,
            success:
            {
                (operation, responseObject) in

                
                self.eatingAtLabel.text = ""
                

                if let navController = self.navigationController {
                    navController.popViewController(animated: true)
                }
                
//                let responseObjectDicto = (responseObject as AnyObject) as! NSDictionary
//                let orderedDicto = responseObjectDicto.sorted(by: { (a, b) in (a.value as! Double) > (b.value as! Double) })
//                
//                
//                for (key,value) in orderedDicto {
//                    print("\(key) = \(value)")
//                    
//                    self.eatingAtLabel.text = "\(key) with \(value) votes"
//                    
//                    //send notification
//                    self.sendSMS()
//                    
//                    break
//                }
                
                
        },
            failure:
            {
                (operation, error) in
                print("Error: " + error.localizedDescription)
                
                if let navController = self.navigationController {
                    navController.popViewController(animated: true)
                }
                
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
