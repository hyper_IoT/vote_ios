

How to run,

Just make sure the pods are installed and working [pods added on repo so should be fine]

a. Platform and language chosen
    - this solutions was made with Swift for the iOS front end. And NodeJS lambda AWS, for the back end

b. What are the highlights of your logic/code writing style?
    - API for restaurants is from YELP, [simulated location Surry hills]
    - Swift app fetches the nearby restaurants [simulated location Surry hills]
    - back end made with AWS serverless architecture: dynamoDB + API gateway + lambda
        - find lambda files on \lambdas
    - a vote is tied to a username, only one vote can be done per session
    - sessions are managed on the front end app, by tapping on "Settings" button
    - a notification SMS is sent to the entered number on "settings"
    - winner restaurant is determined on "settings" 
    - clear of votes is done on "settings"

c. What could have been done in a better way?
    - use real time checker for votes
    - use time based vote counter [1pm requirement]
    - extend DB to handle votes per week of restaurant

d. Any other notes you judge relevant for the evaluation of your project.
    - no time management done on back end, is simulated on ios APP on "settings"
    - GPS simulated [Surry hills]

Lambdas available on GIT also


